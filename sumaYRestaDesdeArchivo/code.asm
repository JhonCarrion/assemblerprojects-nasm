%macro escribir 2
    mov eax, 4
    mov ebx, 2
    mov ecx, %1
    mov edx, %2
    int 80H
%endmacro

section .data
    mensaje1 db 10, 'Leer el archivo', 10
    len1 equ $-mensaje1

    msg db 10, 'La suma es: ', 10
    len equ $ - msg
    msg_resta db 10, 'La resta es: ', 10
    len_resta equ $ - msg_resta

    nlinea db 10, 10, 0
	lnlinea equ $ - nlinea

    a1 db '/home/alex/Asemblerprojects/sumaYRestaDesdeArchivo/a1.txt', 0
    a2 db '/home/alex/Asemblerprojects/sumaYRestaDesdeArchivo/a2.txt', 0
    sum db '     '
    dif db '     '

section .bss
    num1 resb 4
    num2 resb 4
    idarchivo1 resd 1
    idarchivo2 resd 1

section .text
    global _start

_start:
   ;abrir Archivo
    mov eax, 5 ;Servicio para abrir el archivo
    mov ebx, a1 ;direccion el archivo
    mov ecx, 0 ;modo de acceso al archivo, read only = 0
    mov edx, 0 ;permisos del archivo
    int 0x80 ; interrupcion 80H

    ;verificar si el path es correcto
    test eax, eax
    jz salir

    ;archivo sin excepciones
    mov dword [idarchivo1], eax ;respaldo del identificador del archivo

    escribir mensaje1, len1

    mov eax, 3
    mov ebx, [idarchivo1] ; entrada estandar
    mov ecx, num1
    mov edx, 25
    int 0x80

    escribir num1, 4

   ;abrir Archivo
    mov eax, 5 ;Servicio para abrir el archivo
    mov ebx, a2 ;direccion el archivo
    mov ecx, 0 ;modo de acceso al archivo, read only = 0
    mov edx, 0 ;permisos del archivo
    int 0x80 ; interrupcion 80H

    ;verificar si el path es correcto
    test eax, eax
    jz salir

    ;archivo sin excepciones
    mov dword [idarchivo2], eax ;respaldo del identificador del archivo

    escribir mensaje1, len1

    mov eax, 3
    mov ebx, [idarchivo2] ; entrada estandar
    mov ecx, num2
    mov edx, 25
    int 0x80

    escribir num2, 4

    ;**************Inicio Suma de varios Digitos**************
    mov ecx, 4
    mov esi, 3
    clc                  ;instruccion que encera el carry flag, CF = 0

ciclo_suma:
    mov al, [num1 + esi]
    adc al, [num2 + esi] ; adc ademas de sumar los operandos suma el valor del CarryFlag 0 o 1
    aaa                  ; volver al estado original del carry en sumas
    pushf                ; Guarda en pila todos los valores de las banderas

    or al, 0x30          ; 30H vale 48 en decimal(codigo ascii) es como hacer add al, '0'

    popf                 ; rescatar el valor de las banderas y regresarlas a su estado original

    mov [sum + esi], al
    dec esi

    loop ciclo_suma
    escribir msg, len
    escribir sum, 3

    mov ecx, 4
    mov esi, 3
    clc                  ;instruccion que encera el carry flag, CF = 0

ciclo_resta:
    mov al, [num1 + esi]
    sbb al, [num2 + esi] ; adc ademas de sumar los operandos suma el valor del CarryFlag 0 o 1
    aas                  ; volver al estado original del carry en sumas
    pushf                ; Guarda en pila todos los valores de las banderas

    or al, 0x30          ; 30H vale 48 en decimal(codigo ascii) es como hacer add al, '0'

    popf                 ; rescatar el valor de las banderas y regresarlas a su estado original

    mov [dif + esi], al
    dec esi

    loop ciclo_resta

    escribir msg_resta, len_resta
    escribir dif, 3

salir:
    escribir nlinea, lnlinea
    mov eax, 1
    int 0x80
