%macro escribir 2
    mov eax, 4
	mov ebx, 1
	mov ecx, %1
	mov edx, %2
	int 0x80
%endmacro

section .data
    msj db 10, 'Programa que cambia los permisos de un archivo', 10
    len EQU $ - msj

    msj2 db 10, 'Permiso Cambiado', 10
    len2 EQU $ - msj2

    msj3 db 10, 'Permiso Denegado!!!', 10
    len3 EQU $ - msj3

    msj4 db 10, 'Texto Leido:', 10
    len4 EQU $ - msj4

    msj5 db 10, 'Ingrese un texto de menos de 30 caracteres', 10
    len5 EQU $ - msj5

    msj6 db 10, 'Archivo Escrito', 10
    len6 EQU $ - msj6

    opt db 10, 'Escoga una opción', 10
    lenOpt EQU $ - opt

    opt1 db 10, '1 -> chmod 777', 10
    lenOpt1 EQU $ - opt1

    opt2 db 10, '2 -> chmod 775', 10
    lenOpt2 EQU $ - opt2

    opt3 db 10, '3 -> chmod 760', 10
    lenOpt3 EQU $ - opt3

    opt4 db 10, '4 -> chmod 644', 10
    lenOpt4 EQU $ - opt4

    opt5 db 10, '5 -> chmod 640', 10
    lenOpt5 EQU $ - opt5

    opt6 db 10, '6 -> chmod 600', 10
    lenOpt6 EQU $ - opt6

    opt7 db 10, '7 -> chmod 700', 10
    lenOpt7 EQU $ - opt7

    pathread db '/home/alex/Asemblerprojects/chmod/archivo.txt', 0
    lenpath EQU $ - pathread

section .bss
    opcion resb 1
    texto resb 30
    txtescribir resb 30
    idarchivoread resd 1

section .text
    global _start

_start:
    escribir msj, len
    escribir opt, lenOpt
    escribir opt1, lenOpt1
    escribir opt2, lenOpt2
    escribir opt3, lenOpt3
    escribir opt4, lenOpt4
    escribir opt5, lenOpt5
    escribir opt6, lenOpt6
    escribir opt7, lenOpt7

    mov ebx, 0
	mov ecx, opcion
	mov edx, 2
	mov eax, 3
	int 80h

    mov ah, [opcion]; movemos la seleccio a ah
    sub ah, '0'; Convierte la cadena en #

    cmp ah, 1
    je permiso777

    cmp ah, 2
    je permiso775

    cmp ah, 3
    je permiso760

    cmp ah, 4
    je permiso644

    cmp ah, 5
    je permiso640

    cmp ah, 6
    je permiso600

    cmp ah, 7
    je permiso700

operaciones:
    ; ##################################### READ ################################################################
    ;*****   apertura del archivo
    mov eax, 5      ;servicio para abrir el archivo
    mov ebx, pathread   ;servicio de direccion del archivo
    mov ecx, 0      ; modo de acceso, read only = 0
    mov edx, 0      ; permisos del archivo
    int 80H        ; igual al int 80H

    ; ** verificamos si el path es correcto o si existe
    test eax, eax
    jz denegado

    ; **** archivo sin porblemas(excepciones) ****
    mov dword [idarchivoread], eax  ; respaldo el id del archivo

    mov eax, 3
    mov ebx, [idarchivoread]      ; entrada estandar. (0,1,2 ) => entrada por teclado
    mov ecx, texto
    mov edx, 30
    int 80H

    escribir msj4, len4
    escribir texto, 30

    ; ##################################### START WRITE ###########################################################################
    escribir msj5, len5

    mov eax, 3
    mov ebx, 2      ; entrada estandar. (0,1,2 ) => entrada por teclado
    mov ecx, txtescribir
    mov edx, 30
    int 0x80

    ;*****   apertura del archivo
    mov eax, 8          ;servicio para crear y escribir en archivo
    mov ebx, pathread   ;servicio de direccion del archivo
    mov ecx, 2          ; modo de acceso, write and read = 2
    mov edx, 0x1FF      ;permisos
    int 0x80            ; igaul al int 80H

    ; ** verificamos si el path es correcto o si existe
    test eax, eax   ; test es un and sin modificar sus operandos, solo modifica  banderas
    jz denegado

    ; **** archivo sin porblemas(excepciones) ****
    mov dword [idarchivoread], eax  ; respaldo el id del archivo

    mov eax, 4
    mov ebx, [idarchivoread]      ; entrada estandar
    mov ecx, txtescribir
    mov edx, 25
    int 0x80

    ;*****   cerrar del archivo ***********************
    mov eax, 6              ;servicio para cerrar el archivo
    mov ebx, [idarchivoread];servicio de direccion del archivo
    mov ecx, 0              ; no se necesita modo de acceso
    mov edx, 0              ; no se necesita permisos
    int 0x80                ; igaul al int 80H

    escribir msj6, len6

    ; ##################################### END WRITE ###########################################################################

    jmp salir

denegado:
    escribir msj3, len3
    jmp salir

; ##################################### START CHMOD ################################################################

permiso777:
    mov eax, 15           ;servicio para chmod
    mov ebx, pathread     ;servicio de direccion del directorio
    mov ecx, 0x1FF
    mov edx, 0x1FF
    int 0x80              ; igaul al int 80H

    escribir msj2, len2

    jmp operaciones

permiso775:
    mov eax, 15           ;servicio para chmod
    mov ebx, pathread     ;servicio de direccion del directorio
    mov ecx, 0x1FD
    mov edx, 0x1FD
    int 0x80              ; igaul al int 80H

    escribir msj2, len2

    jmp operaciones

permiso760:
    mov eax, 15           ;servicio para chmod
    mov ebx, pathread     ;servicio de direccion del directorio
    mov ecx, 0x1F0
    mov edx, 0x1F0
    int 0x80              ; igaul al int 80H

    escribir msj2, len2

    jmp operaciones

permiso644:
    mov eax, 15           ;servicio para chmod
    mov ebx, pathread     ;servicio de direccion del directorio
    mov ecx, 0x1A4
    mov edx, 0x1A4
    int 0x80              ; igaul al int 80H

    escribir msj2, len2

    jmp operaciones

permiso640:
    mov eax, 15           ;servicio para chmod
    mov ebx, pathread     ;servicio de direccion del directorio
    mov ecx, 0x1A0
    mov edx, 0x1A0
    int 0x80              ; igaul al int 80H

    escribir msj2, len2

    jmp operaciones

permiso600:
    mov eax, 15           ;servicio para chmod
    mov ebx, pathread     ;servicio de direccion del directorio
    mov ecx, 0x180
    mov edx, 0x180
    int 0x80              ; igaul al int 80H

    escribir msj2, len2

    jmp operaciones

permiso700:
    mov eax, 15           ;servicio para chmod
    mov ebx, pathread     ;servicio de direccion del directorio
    mov ecx, 0x1C0
    mov edx, 0x1C0
    int 0x80              ; igaul al int 80H

    escribir msj2, len2

    jmp operaciones

; ##################################### END CHMOD ################################################################

salir:
    mov eax, 1
    mov ebx, 0
    int 80H
