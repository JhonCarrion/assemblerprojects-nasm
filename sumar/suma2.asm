section .data
    a db "ingrese un numero", 10
    len_a equ $ - a
    b db "ingrese otro numero", 10
    len_b equ $ - b

    resultado db "La suma es: ", 10
    len equ $ - resultado
    
    newline db "", 10
    
section .bss
    aa resb 1
    bb resb 1
    suma resb 1
    
section .text
    global _start
    
_start:
;ingresar numeros y almacenarlos 
    mov eax, 04
    mov ebx, 01
    mov ecx, a
    mov edx, len_a
    int 80H
    mov eax, 03
    mov ebx, 02
    mov ecx, aa
    mov edx, 02
    int 80H
    
    mov eax, 04
    mov ebx, 01
    mov ecx, b
    mov edx, len_b
    int 80H
    mov eax, 03
    mov ebx, 02
    mov ecx, bb
    mov edx, 02
    int 80H
;mover los resultados
    mov ax, [aa]
    mov bx, [bb]
    sub ax, '0';convertir cadena a numeros
    sub bx, '0';convertir cadena a numeros
    add ax, bx;ax <- ax+bx
    
    add ax, '0';convertir numero a cadena
    add bx, '0';convertir numero a cadena
    mov [suma], ax;ingresar en la variable suma el valor de ax(resultado de la suma)
    
    mov eax, 04
    mov ebx, 01
    mov ecx, resultado
    mov edx, len
    int 80H
    
    mov eax, 04
    mov ebx, 01
    mov ecx, suma
    mov edx, 02
    int 80H
    
    mov eax, 04
    mov ebx, 01
    mov ecx, newline
    mov edx, 1
    int 80H
    
    mov eax, 01
    int 80H
    
    