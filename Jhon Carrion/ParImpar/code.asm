section .data
	section .data
	msj_a: db 'Ingerse un numero:',0xA
	len_a: equ $-msj_a
	msj_p: db 10, 'El numero es par',0xA
	len_p: equ $-msj_p
	msj_i: db 10, 'El numero es impar',0xA
	len_i: equ $-msj_i
	msj_f: db 10, ' ',0xA
	len_f: equ $-msj_f
    
    nlinea db 10, 10, 0
	lnlinea equ $ - nlinea
	
section .bss
	a: resb 1
	
section .text
	global _start
	
_start:
	mov eax,04
	mov ebx,01
	mov ecx,msj_a
	mov edx,len_a
	int 80H
	
	mov eax,03
	mov ebx,02
	mov ecx,a
	mov edx,2
	int 80H
	
	mov ebx,[a]
	sub ebx,'0'
	and ebx,1
	jz par
	jmp impar
	
par:
    mov eax,04
    mov ebx,01
    mov ecx,msj_p
    mov edx,len_p
    int 80H

    jmp salir

impar:
    mov eax,04
    mov ebx,01
    mov ecx,msj_i
    mov edx,len_i
    int 80H

    jmp salir

salir:
	mov eax, 4
	mov ebx, 1
	mov ecx, nlinea
	mov edx, lnlinea
	int 80h
 
	mov eax, 1
	mov ebx, 0
	int 80h