section .data
	msg2 dd	10, 'Numero 1: '
	lmsg2 equ $ - msg2
 
	msg3 dd	'Numero 2: '
	lmsg3 equ $ - msg3
 
	msg4 db	10, '1. Sumar', 10, 0
	lmsg4 equ $ - msg4
 
	msg9 db	10, 'Resultado: ', 10
	lmsg9 equ $ - msg9
 
	nlinea db 10, 10, 0
	lnlinea equ $ - nlinea
 
section .bss
 
	; Espacios en la memoria reservados para almacenar los valores introducidos por el usuario y el resultado de la operacion.
	num1: resb 3
	num2: resb 3
	resultado: resb 3
 
section .text
 
	global _start
 
_start: 
	; Imprimimos en pantalla el mensaje 2
	mov eax, 4
	mov ebx, 1
	mov ecx, msg2
	mov edx, lmsg2
	int 80h
 
	; Obtenemos el numero 1
	mov eax, 3
	mov ebx, 0
	mov ecx, num1
	mov edx, 3
	int 80h
 
	; Imprimimos en pantalla el mensaje 3
	mov eax, 4
	mov ebx, 1
	mov ecx, msg3
	mov edx, lmsg3
	int 80h
 
	; Obtenemos el numero 2
	mov eax, 3
	mov ebx, 0
	mov ecx, num2
	mov edx, 3
	int 80h
 
	jmp sumar
 
sumar:
	; Movemos los numeros ingresados a los registro AL y BL
	mov al, [num1 + 0]
	mov bl, [num2 + 0]
 
	; Convertimos los valores ingresados de ascii a decimal
	sub al, '0'
	sub bl, '0'
 
	; Sumamos el registro AL y BL
	add al, bl
 
	; Convertimos el resultado de la suma de decimal a ascii
	add al, '0'
 
	; Movemos el resultado a un espacio reservado en la memoria
	mov [resultado], al
 
	; Imprimimos en pantalla el mensaje 9
	mov eax, 4
	mov ebx, 1
	mov ecx, msg9
	mov edx, lmsg9
	int 80h
 
	; Imprimimos en pantalla el resultado
	mov eax, 4
	mov ebx, 1
	mov ecx, resultado
	mov edx, 2
	int 80h
 
    ; Movemos los numeros ingresados a los registro AL y BL
	mov al, [num1 + 1]
	mov bl, [num2 + 1]
 
	; Convertimos los valores ingresados de ascii a decimal
	sub al, '0'
	sub bl, '0'
 
	; Sumamos el registro AL y BL
	add al, bl
 
	; Convertimos el resultado de la suma de decimal a ascii
	add al, '0'
 
	; Movemos el resultado a un espacio reservado en la memoria
	mov [resultado], al
    
    ; Imprimimos en pantalla el resultado
	mov eax, 4
	mov ebx, 1
	mov ecx, resultado
	mov edx, 2
	int 80h
 
	; Finalizamos el programa
	jmp salir
 
salir:
	; Imprimimos en pantalla dos nuevas lineas
	mov eax, 4
	mov ebx, 1
	mov ecx, nlinea
	mov edx, lnlinea
	int 80h
 
	; Finalizamos el programa
	mov eax, 1
	mov ebx, 0
	int 80h