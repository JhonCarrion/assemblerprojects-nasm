section .data
    ;constantes assign define equ
    %assign signos "*"
    msj db "Hola Clase", signos, signos, signos, 10
    len equ $ - msj
    
section .text
    global _start
    
_start:
    ;imprimir 
    mov eax, 04
    mov ebx, 01
    mov ecx, msj
    mov edx, len
    int 80H
    ;salir
    mov eax, 01
    int 80H
    
    