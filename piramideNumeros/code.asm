section .data
    msj_a: db 10, '**'
    ln_a equ $ - msj_a

    b db 10, "ingrese un numero", 10
    len_b equ $ - b

    nlinea db 10, 10, 0
    lnlinea equ $ - nlinea

section .bss
    num resb 1

section .text
    global _start

_start:
    mov eax, 4
    mov ebx, 1
    mov ecx, b
    mov edx, len_b
    int 80H

    mov eax, 03
    mov ebx, 02
    mov ecx, num
    mov edx, 2
    int 80H
    
    mov eax, [num]
    sub eax, '0'
	mov ecx, eax

vertical:
	push ecx
	
    mov eax, ecx
    loop horizontal
    
	pop ecx
    loop vertical
    jmp salir

horizontal:
	push eax
	add eax, '0'
	mov [msj_a], eax

	mov eax, 4
	mov ebx, 1
	mov ecx, msj_a
	mov edx, ln_a
	int 80H

	pop eax
    mov ecx, eax
    push ecx
    sub eax, 1
	cmp eax, 0
    jz vertical
	jmp horizontal


salir:
	mov eax, 4
	mov ebx, 1
	mov ecx, nlinea
	mov edx, lnlinea
	int 80h

	mov eax, 1
	mov ebx, 0
	int 80h