section .data
	msj_f: db 10, ' ', 0
	len_f: equ $-msj_f
    
    nlinea db 10, 10, 0
	lnlinea equ $ - nlinea
	
section .bss
	r: resb 1
	
section .text
	global _start
	
_start:    
    mov ecx, 09
    
principal:
    cmp ecx, 0
    jz salir
    jmp imprimir
    
imprimir:
    dec ecx 
    push ecx
    
    mov ax, '*'
    mov [r], ax
    mov eax, 04
	mov ebx, 01
	mov ecx, r
	mov edx, 1
	int 80H
    pop ecx
    jmp principal

salir:
	mov eax, 4
	mov ebx, 1
	mov ecx, nlinea
	mov edx, lnlinea
	int 80h
 
	mov eax, 1
	mov ebx, 0
	int 80h