section .data
    msj db "Hola Clase", 10
    len equ $ - msj
    ;constantes assign define equ
    
section .text
    global _start
    
_start:
    mov ax, 02
    mov bx, 05
    mov [msj], dword 'mala'
    
    mov eax, 04
    mov ebx, 01
    mov ecx, msj
    mov edx, len
    int 80H
    
    mov ax, 02
    mov bx, 05
    mov [msj + 5], dword 'nina'
    
    mov eax, 04
    mov ebx, 01
    mov ecx, msj
    mov edx, len
    int 80H
    
    mov eax, 01
    int 80H
    
    