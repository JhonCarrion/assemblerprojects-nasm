%macro escribir 2
    mov eax, 4
    mov ebx, 2
    mov ecx, %1
    mov edx, %2
    int 80H
%endmacro

section .data
    mensaje1 db 10, 'Leer el archivo', 10
    len1 equ $-mensaje1

    arreglo db 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
    ln_arreglo equ $ - arreglo

    nlinea db 10, 10, 0
	lnlinea equ $ - nlinea

    a1 db '/home/alex/Asemblerprojects/repeticiones/datos.txt', 0
    sum db '     '

section .bss
    num1 resb 4
    idarchivo1 resd 1

section .text
    global _start

_start:
   ;abrir Archivo
    mov eax, 5 ;Servicio para abrir el archivo
    mov ebx, a1 ;direccion el archivo
    mov ecx, 0 ;modo de acceso al archivo, read only = 0
    mov edx, 0 ;permisos del archivo
    int 0x80 ; interrupcion 80H

    ;verificar si el path es correcto
    test eax, eax
    jz salir

    ;archivo sin excepciones
    mov dword [idarchivo1], eax ;respaldo del identificador del archivo

    escribir mensaje1, len1

    mov eax, 3
    mov ebx, [idarchivo1] ; entrada estandar
    mov ecx, num1
    mov edx, 25
    int 0x80

    ;mov [esi], num1
    ;inc esi
    ;inc edi

    escribir num1, 4

    ;**************Inicio Suma de varios Digitos**************
    mov ecx, 4
    mov esi, 3
    clc                  ;instruccion que encera el carry flag, CF = 0

ciclo_suma:
    mov al, [num1 + esi]
    aas                  ; volver al estado original del carry en sumas
    pushf                ; Guarda en pila todos los valores de las banderas

    or al, 0x30          ; 30H vale 48 en decimal(codigo ascii) es como hacer add al, '0'

    popf                 ; rescatar el valor de las banderas y regresarlas a su estado original

    mov [sum + esi], al
    dec esi

    loop ciclo_suma

    escribir sum, 3

salir:
    escribir nlinea, lnlinea
    mov eax, 1
    int 0x80
