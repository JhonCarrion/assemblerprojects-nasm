section .data
    a db "ingrese un numero", 10
    len_a equ $ - a
    b db "ingrese otro numero", 10
    len_b equ $ - b

    resultado db "iguales: si", 10
    len equ $ - resultado
    
    newline db "", 10
    
section .bss
    numa resb 1
    numb resb 1
    res resb 1
    
section .text
    global _start
    
_start:
;ingresar numeros y almacenarlos 
    mov eax, 04
    mov ebx, 01
    mov ecx, a
    mov edx, len_a
    int 80H
    
    mov eax, 03
    mov ebx, 02
    mov ecx, numa
    mov edx, 02
    int 80H
    
    mov eax, 04
    mov ebx, 01
    mov ecx, b
    mov edx, len_b
    int 80H
    
    mov eax, 03
    mov ebx, 02
    mov ecx, numb
    mov edx, 02
    int 80H
    
    mov ah, [numa]	; Movemos la opcion seleccionada a el registro AH
	sub ah, '0'		; Convertimos el valor ingresado de ascii a decimal
    
    mov bh, [numb]	; Movemos la opcion seleccionada a el registro BH
	sub bh, '0'		; Convertimos el valor ingresado de ascii a decimal
    
    cmp ah, bh ;se alteran las baderas ZR, si exite igualdad
    jz igual
    jmp no_igual 
    
igual:
    mov ax, 02
    mov bx, 05
    mov [resultado + 9], dword 'si'
    
    ; Finalizamos el programa
	jmp salir
    
no_igual:
    mov ax, 02
    mov bx, 05
    mov [resultado + 9], dword 'no'
    
    ; Finalizamos el programa
	jmp salir
    
salir:
    mov eax, 04
    mov ebx, 01
    mov ecx, resultado
    mov edx, len
    int 80H
    
    mov eax, 04
    mov ebx, 01
    mov ecx, res
    mov edx, 01
    int 80H

	; Imprimimos en pantalla dos nuevas lineas
	mov eax, 4
	mov ebx, 1
	mov ecx, newline
	mov edx, 01
	int 80h
 
	; Finalizamos el programa
	mov eax, 1
	mov ebx, 0
	int 80h