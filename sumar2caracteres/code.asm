%macro escribir 2
    mov eax, 4
    mov ebx, 2
    mov ecx, %1
    mov edx, %2
    int 80H
%endmacro

section .data
    msg db 'La suma es: ', 10
    len equ $ - msg

    nlinea db 10, 10, 0
	lnlinea equ $ - nlinea

    num1 db '518'
    num2 db '197'
    sum db '   '

section .text
    global _start

_start:
    ;**************A tener en Cuenta**************
    mov eax, 5

    add eax, '0'         ; en el codigo ascii el 0 vale 48
    add eax, 48          ; funciona igual que add eax, '0' para convertir ascci a decimal

    escribir eax, 2      ; operaciones de suma simples 0-9
    ;**************Fin A tener en Cuenta**************
    ;**************Inicio Suma de varios Digitos**************
    mov ecx, 3
    mov esi, 2
    clc                  ;instruccion que encera el carry flag, CF = 0

ciclo_suma:
    mov al, [num1 + esi]
    adc al, [num2 + esi] ; adc ademas de sumar los operandos suma el valor del CarryFlag 0 o 1
    aaa                  ; volver al estado original del carry en sumas
    pushf                ; Guarda en pila todos los valores de las banderas

    or al, 0x30          ; 30H vale 48 en decimal(codigo ascii) es como hacer add al, '0'

    popf                 ; rescatar el valor de las banderas y regresarlas a su estado original

    mov [sum + esi], al
    dec esi

    loop ciclo_suma

    escribir msg, len
    escribir sum, 3

salir:
    escribir nlinea, lnlinea
    mov eax, 1
    int 0x80
