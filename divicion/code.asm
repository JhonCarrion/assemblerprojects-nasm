section .data
    a db "ingrese un numero", 10
    len_a equ $ - a
    b db "ingrese otro numero", 10
    len_b equ $ - b

    resultado db "El cociente es: ", 10
    len equ $ - resultado
    resi db "El residuo es: ", 10
    len_r equ $ - resi
    
    newline db "", 10
    
section .bss
    dividendo resb 1
    divisor resb 1
    cociente resb 1
    residuo resb 1
    
section .text
    global _start
    
_start:
;ingresar numeros y almacenarlos 
    mov eax, 04
    mov ebx, 01
    mov ecx, a
    mov edx, len_a
    int 80H
    mov eax, 03
    mov ebx, 02
    mov ecx, dividendo
    mov edx, 02
    int 80H
    
    mov eax, 04
    mov ebx, 01
    mov ecx, b
    mov edx, len_b
    int 80H
    mov eax, 03
    mov ebx, 02
    mov ecx, divisor
    mov edx, 02
    int 80H
    ;mover los resultados
    mov al, [dividendo]
    mov bl, [divisor]
    sub al, '0';convertir cadena a numeros
    sub bl, '0';convertir cadena a numeros
    div bl;ax <- ax/bx al contiene el cociente, ah contiene el residuo
    
    
    add al, '0';convertir numero a cadena
    add ah, '0';convertir numero a cadena
    mov [cociente], al;ingresar en la variable suma el valor de ax(resultado de la suma)
    mov [residuo], ah;ingresar en la variable suma el valor de ax(resultado de la suma)
    
    mov eax, 04
    mov ebx, 01
    mov ecx, resultado
    mov edx, len
    int 80H
    
    mov eax, 04
    mov ebx, 01
    mov ecx, cociente
    mov edx, 01
    int 80H
    
    mov eax, 04
    mov ebx, 01
    mov ecx, newline
    mov edx, 1
    int 80H
        
    mov eax, 04
    mov ebx, 01
    mov ecx, resi
    mov edx, len_r
    int 80H
    
    mov eax, 04
    mov ebx, 01
    mov ecx, residuo
    mov edx, 01
    int 80H
        
    mov eax, 04
    mov ebx, 01
    mov ecx, newline
    mov edx, 1
    int 80H
    
    mov eax, 01
    int 80H
    
    