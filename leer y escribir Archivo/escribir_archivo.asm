%macro escribir 2
    mov eax, 4
    mov ebx, 2
    mov ecx, %1
    mov edx, %2
    int 80H
%endmacro
section .data
    mensaje1 db 10, 'Escribir archivo', 10
    len1 equ $-mensaje1
    
    path db "/home/alex/Asemblerprojects/calculadora/archivob1.txt", 0
    
section .bss
    texto resb 30
    idarchivo resd 1
    
section .text
    global _start
    
_start:
    ;abrir Archivo
    mov eax, 8 ;Servicio para Crear y escribir el archivo
    mov ebx, path ;direccion el archivo
    mov ecx, 1 ;modo de acceso al archivo, write only = 0
    mov edx, 777h ;permisos del archivo
    int 0x80 ; interrupcion 80H
    
    ;verificar si el path es correcto
    test eax, eax ; test es un and sin modificar sus operandos, solo modifica banderas
    jz salir
    
    ;archivo sin excepciones
    mov dword [idarchivo], eax ;respaldo del identificador del archivo
    
    escribir mensaje1, len1
    
    mov eax, 3
    mov ebx, 2      
    mov ecx, texto 
    mov edx, 25     
    int 0x80
    
    mov eax, 4
    mov ebx, [idarchivo]      
    mov ecx, texto 
    mov edx, 25     
    int 0x80
    
    ;Cerrar Archivo
    mov eax, 6 ;Servicio para cerrar el archivo
    mov ebx, [idarchivo] ;identificador del archivo
    mov ecx, 0 ;modo de acceso al archivo, read only = 0
    mov edx, 0 ;permisos del archivo
    int 0x80 ; interrupcion 80H
    
salir:
    mov eax, 1
    int 80h

    

    
    
    