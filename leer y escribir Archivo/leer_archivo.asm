%macro escribir 2
    mov eax, 4
    mov ebx, 2
    mov ecx, %1
    mov edx, %2
    int 80H
%endmacro
section .data
    mensaje1 db 10, 'Leer el archivo', 10
    len1 equ $-mensaje1
    
    path db "/home/alex/Asemblerprojects/calculadora/archivob.txt", 0
    
section .bss
    texto resb 30
    idarchivo resd 1
    
section .text
    global _start
    
_start:
    ;abrir Archivo
    mov eax, 5 ;Servicio para abrir el archivo
    mov ebx, path ;direccion el archivo
    mov ecx, 0 ;modo de acceso al archivo, read only = 0
    mov edx, 0 ;permisos del archivo
    int 0x80 ; interrupcion 80H
    
    ;verificar si el path es correcto
    test eax, eax
    jz salir
    
    ;archivo sin excepciones
    mov dword [idarchivo], eax ;respaldo del identificador del archivo
    
    escribir mensaje1, len1
    
    mov eax, 3
    mov ebx, [idarchivo] ; entrada estandar       
    mov ecx, texto 
    mov edx, 25     
    int 0x80
    
    escribir texto, 25
    
    ;Cerrar Archivo
    mov eax, 6 ;Servicio para abrir el archivo
    mov ebx, [idarchivo] ;identificador del archivo
    mov ecx, 0 ;modo de acceso al archivo, read only = 0
    mov edx, 0 ;permisos del archivo
    int 0x80 ; interrupcion 80H
    
salir:
    mov eax, 1
    int 80h

    

    
    
    