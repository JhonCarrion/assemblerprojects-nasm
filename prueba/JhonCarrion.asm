section .data
;evaluacion 5 Jhon Alexander Carrión Piedra 6B-CIS
    hello db "Esta es la Prueba #5 @author: Jhon A. Carrión P.", 10
    len_hello equ $ - hello
    a db "ingrese un numero", 10
    len_a equ $ - a
    b db "ingrese otro numero", 10
    len_b equ $ - b

    resultado db "El cociente es: ", 10
    len equ $ - resultado
    resi db "El residuo es: ", 10
    len_r equ $ - resi
    
    newline db "", 10
    
section .bss
    dividendo resb 1
    divisor resb 1
    cociente resb 1
    residuo resb 1
    
section .text
    global _start
    
_start:
;Saludar 
    mov eax, 04
    mov ebx, 01
    mov ecx, hello
    mov edx, len_hello
    int 80H
;ingresar numeros y almacenarlos 
    mov eax, 04
    mov ebx, 01
    mov ecx, a
    mov edx, len_a
    int 80H
    mov eax, 03
    mov ebx, 02
    mov ecx, dividendo
    mov edx, 02
    int 80H
    
    mov eax, 04
    mov ebx, 01
    mov ecx, b
    mov edx, len_b
    int 80H
    mov eax, 03
    mov ebx, 02
    mov ecx, divisor
    mov edx, 02
    int 80H
    ;mover los resultados
    mov ax, [dividendo]
    mov bx, 01
    sub ax, '0';convertir cadena a numeros
    sub bx, '0';convertir cadena a numeros
    ;div bl;ax <- ax/bx al contiene el cociente, ah contiene el residuo
    ;sub ax, [divisor];ax <- ax-bx
    jmp restar
    
comparador:
    cmp ax, 0
    je salir
    ja salir
    jmp restar
    
restar:
    sub ax, [divisor]
    inc bx
    jmp comparador

salir:
    add bx, '0';convertir numero a cadena
    add ax, '0';convertir numero a cadena
    mov [cociente], bx
    mov [residuo], ax
    
	mov eax, 04
    mov ebx, 01
    mov ecx, resultado
    mov edx, len
    int 80H
    
    mov eax, 04
    mov ebx, 01
    mov ecx, cociente
    mov edx, 01
    int 80H
    
    mov eax, 04
    mov ebx, 01
    mov ecx, newline
    mov edx, 1
    int 80H
        
    
    mov eax, 04
    mov ebx, 01
    mov ecx, resi
    mov edx, len_r
    int 80H
    
    mov eax, 04
    mov ebx, 01
    mov ecx, residuo
    mov edx, 01
    int 80H
        
    mov eax, 04
    mov ebx, 01
    mov ecx, newline
    mov edx, 1
    int 80H
 
	mov eax, 1
	mov ebx, 0
	int 80h 
